﻿#
# Runs a content search from C:\secops\newsearch.csv
#
# Edit the csv and place email addresses in the newsearch csv.  One per line.
#
# By Russell Kalani
# v1  10.23.2023
#

$username = ''

#Get current logged in user and build login name
$uName = [Environment]::UserName -split "\."
$logName = $uName[0] + ".oa@creditone.com"

Connect-IPPSSession -UserPrincipalName $logName

$rpath = 'C:\secops'


$path = -join($rpath,'\')

$file = 'newsearch.csv'

$full = -join($path,$file)

$from = 'from:'

#Check for file existence 

if (Test-Path $full) {
#import CSV

    $build = Import-Csv $full -Header email

# loop through addresses and check to see if it exists.  If exists - re-run search, if not start a new one

    foreach ( $name in $build.email ) {

       

        $find = Get-ComplianceSearchAction | Select-Object SearchName, JobEndTime,Status | Where-Object { $_.SearchName -Like -join('*',$name,'*')  }
    
            if ($find -ne $null) {
     
               Write-Host 'Restarting search: ' $find.SearchName
               Start-ComplianceSearch -Identity $find.SearchName

             } else {

                Write-Host 'Creating search: ' (-Join('SP - Sender: ',$Name))
                New-ComplianceSearch -Name (-Join('SP - Sender: ',$Name)) -ExchangeLocation ALL -ContentMatchQuery (-join($from,$name)) | Start-ComplianceSearch 
         
             }
    }

} else {

Clear
Write-Host 'File does not exist ' $full 
Write-Host 'Creating newsearch.csv file in ' $rpath
$null = New-Item -Path $full -ItemType File 
}
Disconnect-ExchangeOnline -Confirm:$false

