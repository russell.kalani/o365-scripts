﻿#
# Runs a content search (purge) of reqular spam email
# From schedsearch.csv
#
#
# By Russell Kalani
# v1  10.23.2023
#
$username = ''

#Get current logged in user and build login name
$uName = [Environment]::UserName -split "\."
$logName = $uName[0] + ".oa@creditone.com"

Connect-IPPSSession -UserPrincipalName $logName

$rpath = 'C:\secops'

$path = -join($rpath,'\')

$file = 'newsearch.csv'

$full = -join($path,$file)

$from = 'from:'
$preText = 'SP - Sender: '

$dte = Get-Date

#Check for file existence 

if (Test-Path $full) {

    #import CSV

    $build = Import-Csv $full -Header email

        foreach ( $name in $build.email ) {

            $fullText = -join($preText,$name)

            $find = Get-ComplianceSearch -Identity $fullText | Select-Object SearchName, JobEndTime,Status 
            
            
            if ( $find.Status -eq 'Completed' ) {

                
                Write-Host "Running Purge for:" $fullText

                New-ComplianceSearchAction -SearchName $fullText -Purge -PurgeType SoftDelete -Confirm:$false

           
                Write-Host "Updating Description: " $dte

                Set-ComplianceSearch -Identity $fullText -Description "Purged $dte"
            
            } else {

                Write-Host "Please check Search: " $fullText
            }
        }

} else {

    Clear
    Write-Host 'File does not exist: ' $full 
}
Disconnect-ExchangeOnline -Confirm:$false