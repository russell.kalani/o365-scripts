﻿ #Connect to PS Exchange Online
 clear-Host

 $toEmail = 'russell.kalani@creditone.com'
 $fromEmail = 'tenantblock@creditone.com'
 $repemail = 'secopsreports@creditone.com'
 
 try {
 
    $uName = [Environment]::UserName -split "\."
    $logName = $uName[0] + ".oa@creditone.com"
    $toMe = get-aduser -Identity $uName[0] | Select-Object -ExpandProperty UserPrincipalName
  
    
        if ($toMe -ne '') {
            
            $toEmail = @($soReports, $toMe)
            Write-Host 'True'   
            Write-Host $toMe   

        } else {

            $toEmail = $toMe
        
        }

    $conn = Connect-ExchangeOnline -UserPrincipalName $logName 
 
 }
 catch {

    Write-Host "Please attempt manual connection to 'Connect-ExchangeOnline'"
   
 }
 finally {
 
    $conn = Get-ConnectionInformation | Where-Object { $_.State -eq "Connected"} 
    $negLine = "List does not contain any *.creditone.com email addresses"
    
    $tenList = Get-TenantAllowBlockListItems -ListType Sender -Block | Where-Object { $_.value -like "*.creditone.com"} | Select-Object Value
    # Uncoment to generate list of the .com items for testing !!@#
    #$tenList = Get-TenantAllowBlockListItems -ListType Sender -Block | Where-Object { $_.value -notlike "*.com"} 

    Write-Host ""
    Write-host "Logged In: $($conn.UserPrincipalName)"
    Write-Host ""
    
    $Line2 = "Generated by: $($conn.UserPrincipalName)" + '<br />'
    

    if ($tenList -eq $null) {

        $whole1 = $Line2  + '<br />' + $negLine
        Send-MailMessage -From $fromEmail -To $toEmail -BodyAsHtml -Subject "Tenant Block List" -Body $whole1 -SmtpServer mailgateway.fnbm.corp

    } else {
        
        $Line0 =  "Please check the Tenant Level Block List and remove the following addresses:" + '<br />'
        $Line1 =  "Link to List: https://security.microsoft.com/tenantAllowBlockList?viewid=Sender&tid=d48cfd66-1099-47e8-898f-6de8401f42b8" + '<br />'
        $LineB =  '<br />'

        $whole1 = $Line0 + '<br />' + '<br />' + $Line1 + '<br />' + $Line2 + '<br />' 

            foreach ($add in $tenlist) {

                $whole = $add.Value 
                $whole1 = $whole1 + '<br />' + $whole
            }


        Send-MailMessage -From $fromEmail -To $toEmail -BodyAsHtml -Subject "Tenant Block List" -Body $whole1 -SmtpServer mailgateway.fnbm.corp
        Write-Host ""
        Write-Host "Email Sent"
    }

Disconnect-ExchangeOnline -Confirm:$false -ErrorAction SilentlyContinue
Write-Host "Disconnected from ExchangeOnline"

}