# O365 Scripts

Three scripts:

    Tenant Search - searches the O365 tenant list for any *creditone.com domains in the list.  
    ** Must manually remove them from the tenant list **  Precautionary step

    SP-Search
        Creates a search item in the Content Search from emails in the newsearch.csv file.  
        newsearch.csv should contain one email address per-line.  No commas necessary.

    SP-Purge
        Uses the newsearch.csv to soft-delete the items in the content search list.
